# ftp-mysql-dump

Simple bash script to dump a MySQL database over ftp.

This script was created to easily retrieve a dump from a webserver database, when there is no ssh available.

Author:

* David Gruner, https://www.gruniversal.de

License:

* Creative Commons BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/

## What is does?

This script generates a PHP file, then uploads it to a webserver and executes it to generate a mysql dump for a specific database.

It uses a random name for the php script to prevent unwanted usage.

It also deletes the script immediately after creating the dump.

## Requirements

To use this script you need:

* local environment:
  * bash
  * wget
  * ftp
* webserver environment:
  * PHP with [ZipArchive](https://www.php.net/manual/de/class.ziparchive.php)
  * access via ftp
  * mysqldump (with access to database)

## Configuration

Create a file `.ftp-mysql-dump` in your project and set the following parameters:

<pre>
#!/bin/bash

DB_HOST='localhost'
DB_NAME='database-name'
DB_USER='database-user-name'
DB_PASS='database-user-password'

FTP_HOST='www.example.com'
FTP_PATH='/path/to/httpdocs'
FTP_USER='ftp-user-name'
FTP_PASS='ftp-user-password'

HTTP_HOST='www.example.com'
HTTP_PATH='/'
</pre>

You can leave parameters empty. The script will then ask for these values.