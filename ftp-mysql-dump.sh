#!/bin/bash
#
# ftp-mysql-dump.sh: dump a MySQL database over ftp
#
# This script generates a PHP file, then uploads it to a webserver and executes
# it to generate a mysql dump for a specific database (all tables).
# 
# It uses a random name for the php script to prevent unwanted usage.
# It also deletes the script immediately after creating the dump.
# 
# @author: David Gruner, https://www.gruniversal.de
# @license: https://creativecommons.org/licenses/by-nc-sa/4.0/
# @see: https://gitlab.com/gruniversal/ftp-mysql-dump
#
#=================================================================================

## default configuration (use .ftp-mysql-dump to setup your configuration)

DB_HOST='localhost'
DB_NAME=''
DB_USER=''
DB_PASS=''

FTP_HOST=''
FTP_PATH=''
FTP_USER=''
FTP_PASS=''

HTTP_HOST=''
HTTP_PATH=''

echo -e "\e[0;33m***\e[0m"
echo -e "\e[0;33m*** ftp-mysql-dump v0.2\e[0m"
echo -e "\e[0;33m***\e[0m"
echo -e "\e[0;33m*** This script generates a PHP file, then uploads it to a webserver and executes\e[0m"
echo -e "\e[0;33m*** it to generate a mysql dump for a specific database.\e[0m"
echo -e "\e[0;33m***\e[0m"
echo

## configuration

if [[ -f ".ftp-mysql-dump" ]]; then
	echo -e "\e[0;33m>>> reading configuration file\e[0m"
	echo ".ftp-mysql-dump"
	source ".ftp-mysql-dump"
fi

echo -e "\e[0;33m>>> mysql configuration\e[0m"
if [[ ${DB_HOST} == "" ]]; then
	echo -ne "DB_HOST: "
	read DB_HOST
else
	echo -e "DB_HOST: ${DB_HOST}"
fi
if [[ ${DB_NAME} == "" ]]; then
	echo -ne "DB_NAME: "
	read DB_NAME
else
	echo -e "DB_NAME: ${DB_NAME}"
fi
if [[ ${DB_USER} == "" ]]; then
	echo -ne "DB_USER: "
	read DB_USER
else
	echo -e "DB_USER: ${DB_USER}"
fi
if [[ ${DB_PASS} == "" ]]; then
	echo -ne "DB_PASS: "
	read DB_PASS
else
	echo -e "DB_PASS: ${DB_PASS}"
fi

echo -e "\e[0;33m>>> ftp configuration\e[0m"
if [[ ${FTP_HOST} == "" ]]; then
	echo -ne "FTP_HOST: "
	read FTP_HOST
else
	echo -e "FTP_HOST: ${FTP_HOST}"
fi
if [[ ${FTP_PATH} == "" ]]; then
	echo -ne "FTP_PATH: "
	read FTP_PATH
else
	echo -e "FTP_PATH: ${FTP_PATH}"
fi
if [[ ${FTP_USER} == "" ]]; then
	echo -ne "FTP_USER: "
	read FTP_USER
else
	echo -e "FTP_USER: ${FTP_USER}"
fi
if [[ ${FTP_PASS} == "" ]]; then
	echo -ne "FTP_PASS: "
	read FTP_PASS
else
	echo -e "FTP_PASS: ${FTP_PASS}"
fi

echo -e "\e[0;33m>>> http configuration\e[0m"
if [[ ${HTTP_HOST} == "" ]]; then
	echo -e "HTTP_HOST not set, using default: FTP_HOST"
	HTTP_HOST="${FTP_HOST}"
else
	echo -e "HTTP_HOST: ${HTTP_HOST}"
fi
if [[ ${HTTP_PATH} == "" ]]; then
	echo -e "HTTP_PATH not set, using default: /"
	HTTP_PATH='/'
else
	echo -e "HTTP_PATH: ${HTTP_PATH}"
fi

## generate php script

echo -e "\e[0;33m>>> generate php script\e[0m"
RAND=`date --rfc-3339=ns | md5sum`
FILE_NAME="ftp-mysql-dump_${RAND:4:8}.php"
DUMP_NAME="ftp-mysql-dump_${RAND:4:8}.sql"
ZIP_NAME="ftp-mysql-dump_${RAND:4:8}.sql.zip"

echo "FILE_NAME: ${FILE_NAME}"
echo "DUMP_NAME: ${DUMP_NAME}"
echo "ZIP_NAME: ${ZIP_NAME}"

PHP_SCRIPT=`cat <<EOF
<?php

# enable for debugging
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);

# create dump
exec("mysqldump --user='${DB_USER}' --password='${DB_PASS}' --host='${DB_HOST}' '${DB_NAME}' --result-file='${DUMP_NAME}' 2>&1", \\$output);

# enable for debugging
#var_dump(\\$output);

# create zipfile
\\$zip = new ZipArchive();
\\$zip->open('${ZIP_NAME}', ZipArchive::CREATE);
\\$zip->addFile('${DUMP_NAME}');
\\$zip->close();

# send zipfile
echo file_get_contents('${ZIP_NAME}');

# delete dump and zip file
unlink('${DUMP_NAME}');
unlink('${ZIP_NAME}');
EOF`
echo "${PHP_SCRIPT}" > ${FILE_NAME}

## upload php script

echo -e "\e[0;33m>>> upload php script\e[0m"
echo "ftp://${FTP_USER}@${FTP_HOST}/${FTP_PATH}/${FILE_NAME}"
ftp -n "${FTP_HOST}" <<EOF
quote USER "${FTP_USER}"
quote PASS "${FTP_PASS}"
cd ${FTP_PATH}
binary
put ${FILE_NAME}
quit
EOF

## execute php script

echo -e "\e[0;33m>>> execute php script\e[0m"
TIMESTAMP=`date +%Y%m%d_%H%M%S`
wget -O dump_${TIMESTAMP}.sql.zip http://${HTTP_HOST}/${HTTP_PATH}/${FILE_NAME}

## delete php script

echo -e "\e[0;33m>>> delete php script from server\e[0m"
echo "ftp://${FTP_USER}@${FTP_HOST}/${FTP_PATH}/${FILE_NAME}"
ftp -n "${FTP_HOST}" <<EOF
quote USER "${FTP_USER}"
quote PASS "${FTP_PASS}"
cd ${FTP_PATH}
binary
delete ${FILE_NAME}
quit
EOF

echo -e "\e[0;33m>>> delete php script locally\e[0m"
rm ${FILE_NAME}
